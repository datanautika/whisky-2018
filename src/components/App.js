import React from 'react';
import $ from 'jquery';

import './Page.css';
import routeStream from '../streams/routeStream';
import styles from './App.css';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import Breadcrumbs from './Breadcrumbs';


export default class App extends React.Component {
	render() {
		return <div className={styles.root}>
			<Header />
			<Breadcrumbs />
			<Main />
			<Footer />
		</div>;
	}

	componentDidMount() {
		routeStream.subscribe(() => {
			this.forceUpdate();
		});
		$('#app').addClass('isLoaded');
	}
}
