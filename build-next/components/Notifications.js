'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCssTransitionGroup = require('react-addons-css-transition-group');

var _reactAddonsCssTransitionGroup2 = _interopRequireDefault(_reactAddonsCssTransitionGroup);

var _Notifications = require('./Notifications.css');

var _Notifications2 = _interopRequireDefault(_Notifications);

var _notificationsStream = require('../streams/notificationsStream');

var _notificationsStream2 = _interopRequireDefault(_notificationsStream);

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const ERROR_NOTIFICATION = _constants2.default.ERROR_NOTIFICATION;
const OK_NOTIFICATION = _constants2.default.OK_NOTIFICATION;
// const IN_PROGRESS_NOTIFICATION = constants.IN_PROGRESS_NOTIFICATION;
const NOTIFICATION_FADEOUT_DELAY = 4000;

let notificationsCount = 0;

let Notifications = function (_React$Component) {
	_inherits(Notifications, _React$Component);

	function Notifications() {
		var _temp, _this, _ret;

		_classCallCheck(this, Notifications);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (Notifications.__proto__ || Object.getPrototypeOf(Notifications)).call(this, ...args)), _this), _this.state = {
			notifications: []
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Notifications, [{
		key: 'render',
		value: function render() {
			let notificationElements = [];

			this.state.notifications.forEach(notification => {
				notificationElements.push(_react2.default.createElement(
					'li',
					{ key: notification.id, className: _Notifications2.default.notification + (notification.type === ERROR_NOTIFICATION ? ' isError' : '') + (notification.type === OK_NOTIFICATION ? ' isSuccess' : '') },
					_react2.default.createElement(
						'p',
						{ className: _Notifications2.default.notificationTitle },
						notification.title
					),
					_react2.default.createElement(
						'p',
						{ className: _Notifications2.default.notificationText },
						notification.text
					)
				));
			});

			return _react2.default.createElement(
				'div',
				{ className: _Notifications2.default.root },
				_react2.default.createElement(
					_reactAddonsCssTransitionGroup2.default,
					{
						component: 'ul',
						transitionName: {
							enter: _Notifications2.default.notificationEnter,
							enterActive: _Notifications2.default.notificationEnterActive,
							leave: _Notifications2.default.notificationLeave,
							leaveActive: _Notifications2.default.notificationLeaveActive
						},
						transitionEnterTimeout: (_config2.default.styles.durations.faster + _config2.default.styles.durations.faster) * 1000,
						transitionLeaveTimeout: (_config2.default.styles.durations.faster + _config2.default.styles.durations.faster) * 1000 },
					notificationElements
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			_notificationsStream2.default.subscribe(notification => {
				let notificationId = ++notificationsCount;

				this.state.notifications.unshift({
					id: notificationId,
					type: notification.type,
					title: notification.title,
					text: notification.text
				});

				setTimeout(() => {
					for (let i = 0; i < this.state.notifications.length; i++) {
						if (this.state.notifications[i].id === notificationId) {
							this.state.notifications.splice(i, 1);

							break;
						}
					}

					this.forceUpdate();
				}, NOTIFICATION_FADEOUT_DELAY);

				this.forceUpdate();
			});
		}
	}]);

	return Notifications;
}(_react2.default.Component);

exports.default = Notifications;