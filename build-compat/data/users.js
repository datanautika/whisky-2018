"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint-disable */

var users = exports.users = [{
  "id": 1,
  "name": "jakubmazanec",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [1]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [2, 3, 4]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [5, 6, 7]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [1, 8, 9]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [10, 11, 12]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [13]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [14, 15]
  }, {
    "categoryName": "Irsko",
    "items": [16]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [17, 18, 19]
  }, {
    "categoryName": "Překvapení roku",
    "items": [20]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [21]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [22]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 2,
  "name": "Hunter",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [23]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [24, 25, 26]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [27, 28, 29]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [30, 31, 32]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [33, 34, 35]
  }, {
    "categoryName": "Blended whisky",
    "items": [36]
  }, {
    "categoryName": "Blended malt",
    "items": [37]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [38]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [39, 40, 41]
  }, {
    "categoryName": "Překvapení roku",
    "items": [23, {
      "id": null,
      "name": "Ron Espero XO",
      "links": ["https://www.rumratings.com/brands/3978-ron-espero-xo"]
    }, 33]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [42, {
      "id": null,
      "name": "Fesslermill 1396 Mettermalt Whisky",
      "links": ["https://fessler-muehle.de/portfolio/mettermalt/"]
    }, 43]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [{
      "id": null,
      "name": "Ron Espero XO",
      "links": ["https://www.rumratings.com/brands/3978-ron-espero-xo"]
    }, 29, 44]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Ron Espero XO",
      "links": ["https://www.rumratings.com/brands/3978-ron-espero-xo"]
    }, {
      "id": null,
      "name": "Dos Maderas Luxus",
      "links": ["https://www.rumratings.com/brands/1134-dos-maderas-luxus"]
    }, {
      "id": null,
      "name": "Diplomático Ambassador Selection",
      "links": ["https://www.rumratings.com/brands/313-diplomatico-ambassador-selection"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Albrecht Secret Enigma Double IPA",
      "description": "",
      "links": ["http://www.pivnici.cz/pivo/albrecht-india-pale-ale-22-secret-enigma/"]
    }, {
      "id": null,
      "name": "Nomád/Permon Zaříkávač chmele Double IPA",
      "description": "",
      "links": ["http://www.pivnici.cz/pivo/nomad-permon-zarikavac-chmele/"]
    }, {
      "id": null,
      "name": "Zichovec Robin American Pale Ale",
      "description": "",
      "links": ["http://www.pivnici.cz/pivo/zichovec-robin/"]
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [{
      "id": null,
      "name": "AleBrowar Rowing Jack IPA, Polsko",
      "links": ["http://www.pivnici.cz/pivo/alebrowar-rowing-jack/"]
    }, {
      "id": null,
      "name": "Pinta A ja pale ale APA, Polsko",
      "links": ["http://www.pivnici.cz/pivo/pinta-a-ja-pale-ale/"]
    }, {
      "id": null,
      "name": "Fourpure Session IPA, Anglie",
      "links": ["http://www.pivnici.cz/pivo/fourpure-session-ipa/"]
    }]
  }]
}, {
  "id": 3,
  "name": "Fileta",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [45]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [46]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [47]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [48]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [49]
  }, {
    "categoryName": "Blended whisky",
    "items": [50]
  }, {
    "categoryName": "Blended malt",
    "items": [13]
  }, {
    "categoryName": "Grain whisky",
    "items": [51]
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [52]
  }, {
    "categoryName": "Japonsko",
    "items": [53]
  }, {
    "categoryName": "Zbytek světa",
    "items": [54]
  }, {
    "categoryName": "Překvapení roku",
    "items": [20]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [55]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [56]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Presidente Martí 15 yo",
      "links": ["http://www.presidente.cz/nase-rumy"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Žatec 12°Export",
      "links": ["http://www.zateckypivovar.cz/produkt/zatec-export"]
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 4,
  "name": "nidan",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [57]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [58, 59, 60]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [61, 62, 63]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [64, 65, 66]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [67, 68, 69]
  }, {
    "categoryName": "Blended whisky",
    "items": [36]
  }, {
    "categoryName": "Blended malt",
    "items": [57, 70, 37]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [71, 72, 73]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": [74, 75, 76]
  }, {
    "categoryName": "Zbytek světa",
    "items": [77, 78, 79]
  }, {
    "categoryName": "Překvapení roku",
    "items": [63, 80, 81]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [82, 83, 84]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [61, 85, 86]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Malt whisky yearbook 2018",
      "links": []
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Samson 12",
      "links": []
    }, {
      "id": null,
      "name": "",
      "description": "",
      "links": []
    }, {
      "id": null,
      "name": "",
      "description": "",
      "links": []
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 5,
  "name": "Spyna",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [87]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [88, 89, 59]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [90, 91, 5]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [92, 93, 94]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [95, 96, 4]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [97]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [98, 99, 100]
  }, {
    "categoryName": "Irsko",
    "items": [101]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [102]
  }, {
    "categoryName": "Překvapení roku",
    "items": [103]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [104]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [22, 105, 106]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Richard Grindal: Duch whisky",
      "links": []
    }, {
      "id": null,
      "name": "Petr Novotný a kolektiv: Pivařka",
      "links": []
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Matuška TROPICAL ROCKET 17°",
      "links": ["http://www.pivovarmatuska.cz/"]
    }, {
      "id": null,
      "name": "Létající CHROUST CrashTest IPA 13°",
      "links": ["http://pivochroust.cz/"]
    }, {
      "id": null,
      "name": "Kamenice nad Lipou IMPERIAL STOUT 22°",
      "links": ["http://www.pivovar-kamenice.cz/"]
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [{
      "id": null,
      "name": "Flying Dog Raging Bitch",
      "links": ["http://www.flyingdog.com/"]
    }, {
      "id": null,
      "name": "BrewDog Punk IPA",
      "links": ["https://www.brewdog.com/"]
    }, {
      "id": null,
      "name": "Flying Dog Imperial IPA",
      "links": ["http://www.flyingdog.com/"]
    }]
  }]
}, {
  "id": 6,
  "name": "lalos",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [107]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [4, 108, 109]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [110, 5, 111]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [107, 112, 113]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [114, 115, 116]
  }, {
    "categoryName": "Blended whisky",
    "items": [117, 118, 119]
  }, {
    "categoryName": "Blended malt",
    "items": [120, 121]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [122]
  }, {
    "categoryName": "Irsko",
    "items": [123]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [124, 125, 20]
  }, {
    "categoryName": "Překvapení roku",
    "items": [{
      "id": null,
      "name": "GlenAllachie Distillery",
      "links": ["https://www.theglenallachie.com/"]
    }, {
      "id": null,
      "name": "Má první návštěva Skotska",
      "links": []
    }]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [126, 127]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [120, 128]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 7,
  "name": "zyki",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [113]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [129, 130, 131]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [132, 5, 133]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [113, 134, 8]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [135, 136, 137]
  }, {
    "categoryName": "Blended whisky",
    "items": [138]
  }, {
    "categoryName": "Blended malt",
    "items": [97, 139, 37]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [140, 141, 142]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [20, 125, 143]
  }, {
    "categoryName": "Překvapení roku",
    "items": []
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": []
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 8,
  "name": "Marvin",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [108, 144, 145]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [146, 147, 148]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [1, 149, 150]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [151, 152, 153]
  }, {
    "categoryName": "Blended whisky",
    "items": [36, 154, 155]
  }, {
    "categoryName": "Blended malt",
    "items": [37, 156, 157]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [158, 159, 160]
  }, {
    "categoryName": "Irsko",
    "items": [161, 162, 163]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": []
  }, {
    "categoryName": "Překvapení roku",
    "items": [113]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [{
      "id": null,
      "name": "Překlad knihy Dave Broom: Světový atlas whisky",
      "links": ["http://www.cpress.cz/svetovy-atlas-whisky-1/"]
    }]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [164]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Dave Broom: Světový atlas whisky",
      "links": ["http://www.cpress.cz/svetovy-atlas-whisky-1/"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Saint James Quintessence",
      "links": ["https://www.rumratings.com/brands/3610-saint-james-quintessence"]
    }, {
      "id": null,
      "name": "Riviere du Mat XO",
      "links": ["https://www.rumratings.com/brands/1947-riviere-du-mat-vieux-traditionnel-xo"]
    }, {
      "id": null,
      "name": "Plantation Barbados 12yo, Single Cask Wild Cherry Finish",
      "links": ["https://www.rumratings.com/brands/4210-plantation-barbados-12-year-single-cask-wild-cherry-finish"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 9,
  "name": "gln",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [165]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [166, 167, 168]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [165, 169, 170]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [171, 172, 173]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [174, 175, 176]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [177, 178]
  }, {
    "categoryName": "Překvapení roku",
    "items": [20]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [179]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [180]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Matuška Hellcat Imperial India Pale Ale",
      "links": ["https://www.ratebeer.com/beer/matuska-hellcat-imperial-india-pale-ale/174588"]
    }, {
      "id": null,
      "name": "Clock No Idols! 15° American IPA",
      "links": ["https://www.ratebeer.com/beer/clock-no-idols-15%C2%B0-american-ipa/294409"]
    }, {
      "id": null,
      "name": "Cobolis Irish Dry Stout",
      "links": ["https://www.ratebeer.com/beer/cobolis-irish-dry-stout-13/631630"]
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [{
      "id": null,
      "name": "Belhaven Scottish Stout",
      "links": ["https://www.ratebeer.com/beer/belhaven-scottish-stout-bottle/99954"]
    }, {
      "id": null,
      "name": "Kasteel Cuvée du Chateau",
      "links": ["https://www.ratebeer.com/beer/kasteel-cuvee-du-chateau/114930"]
    }, {
      "id": null,
      "name": "Bevog Rudeen Black IPA",
      "links": ["https://www.ratebeer.com/beer/bevog-rudeen-black-ipa/317259/"]
    }]
  }]
}, {
  "id": 10,
  "name": "Nethar",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [112]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [181, 182, 129]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [183, 184, 185]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [112, 65, 186]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [80, 187, 188]
  }, {
    "categoryName": "Blended whisky",
    "items": [189, 117]
  }, {
    "categoryName": "Blended malt",
    "items": [70, 37, 190]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [191, 192, 193]
  }, {
    "categoryName": "Irsko",
    "items": [194]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [195, 77, 196]
  }, {
    "categoryName": "Překvapení roku",
    "items": [{
      "id": null,
      "name": "Whisky Show London",
      "links": ["https://whiskyshow.com/london"]
    }]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [197, 198]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Malt Whisky Yearbook 2019",
      "links": ["http://www.maltwhiskyyearbook.com/"]
    }, {
      "id": null,
      "name": "Whiskybraní I",
      "links": ["http://www.whiskybrani.cz/"]
    }, {
      "id": null,
      "name": "Whiskybraní II",
      "links": ["http://www.whiskybrani.cz/"]
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [{
      "id": null,
      "name": "BrewDog Punk IPA",
      "links": ["https://www.brewdog.com/"]
    }, {
      "id": null,
      "name": "BrewDog Jack Hammer",
      "links": ["https://www.brewdog.com/"]
    }, {
      "id": null,
      "name": "BrewDog Elvis Juice",
      "links": ["https://www.brewdog.com/"]
    }]
  }]
}, {
  "id": 11,
  "name": "josé",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [199]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [200, 24, 201]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [202, 203, 204]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [205, 206, 207]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [187, 176, 208]
  }, {
    "categoryName": "Blended whisky",
    "items": [209, 210, 138]
  }, {
    "categoryName": "Blended malt",
    "items": [211, 190, 212]
  }, {
    "categoryName": "Grain whisky",
    "items": [213, 214, 215]
  }, {
    "categoryName": "USA a Kanada",
    "items": [159, 158, 216]
  }, {
    "categoryName": "Irsko",
    "items": [217, 218, 219]
  }, {
    "categoryName": "Japonsko",
    "items": [220, 221, 222]
  }, {
    "categoryName": "Zbytek světa",
    "items": [223, 224, 23]
  }, {
    "categoryName": "Překvapení roku",
    "items": [33, {
      "id": null,
      "name": "limitovaná řada Mackmyra Moment",
      "links": []
    }, 20]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": []
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Don Papa Rare Cask ",
      "links": ["https://rumratings.com/brands/6150-don-papa-rare-cask"]
    }, {
      "id": null,
      "name": "Dos Maderas Luxus",
      "links": ["https://rumratings.com/brands/1134-dos-maderas-luxus"]
    }, {
      "id": null,
      "name": "Dos Maderas PX 5+5 ",
      "links": ["https://rumratings.com/brands/341-dos-maderas-px-5-5"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 12,
  "name": "joy",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [225]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [226, 227, 228]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [148, 229, 230]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [231, 232, 233]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [188, 11, 234]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [15, 100]
  }, {
    "categoryName": "Irsko",
    "items": [null, 235, 236]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [20, 125, 177]
  }, {
    "categoryName": "Překvapení roku",
    "items": [198, 230, {
      "id": null,
      "name": "Uskutečnění prvního ročníku Whisky festivalu v Brně",
      "links": ["https://whiskyfestival.cz/brno/"]
    }]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [237, 238, 239]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [44]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": [{
      "id": null,
      "name": "Sametová sedmnáctka, Beskydský pivovárek",
      "links": ["https://www.beskydskypivovarek.cz/"]
    }, {
      "id": null,
      "name": "Sweet Jesus Oatmeal Imperial Stout, Sibeeria",
      "links": ["http://www.pivnici.cz/pivo/sibeeria-sweet-jesus/"]
    }, {
      "id": null,
      "name": "Zlatá raketa, Matuška",
      "links": ["http://www.pivovarmatuska.cz/nase_pivo.html"]
    }]
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [null, null, {
      "id": null,
      "name": "Sierra Nevada Pale Ale",
      "links": ["http://www.pivnici.cz/pivo/sierra-nevada-pale-ale/"]
    }]
  }]
}, {
  "id": 13,
  "name": "richie",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [107]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [240, 241, 242]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [165, 243, 244]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [171, 245, 32]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [246, 247, 248]
  }, {
    "categoryName": "Blended whisky",
    "items": [249]
  }, {
    "categoryName": "Blended malt",
    "items": [250, 13]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": [125, 20, 251]
  }, {
    "categoryName": "Překvapení roku",
    "items": [252, 153, 253]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [254, 255, 256]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [257, 258, 22]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Whisky Classified",
      "links": ["https://www.bookdepository.com/Whisky-Classified-David-Wishart/9781911595731"]
    }, {
      "id": null,
      "name": "Malt Whisky Yearbook 2019",
      "links": ["http://www.maltwhiskyyearbook.com/"]
    }, {
      "id": null,
      "name": "Whisky",
      "links": ["https://www.knizniklub.cz/knihy/230099-whisky.html"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Dos Maderas 5+5 PX",
      "links": ["https://www.rumratings.com/brands/341-dos-maderas-px-5-5"]
    }, {
      "id": null,
      "name": "Plantation St.Lucia 2004",
      "links": ["https://www.rumratings.com/brands/5210-plantation-st-lucia-2004"]
    }, {
      "id": null,
      "name": "AH Riise XO POrt Cask",
      "links": ["https://www.rumratings.com/brands/2022-a-h-riise-xo-port-cask"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 14,
  "name": "salam15",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [259]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [260, 261, 166]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [262, 183, 263]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [264, 231, 265]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [266, 267, 268]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [269, 97, 270]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": []
  }, {
    "categoryName": "Překvapení roku",
    "items": [187, 261, 259]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [271, 197, 12]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [259, 272, 273]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 15,
  "name": "Senoy",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [274]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [275, 58, 276]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [277, 278, 279]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [274, 107, 280]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [152, 187, 281]
  }, {
    "categoryName": "Blended whisky",
    "items": [189]
  }, {
    "categoryName": "Blended malt",
    "items": [282]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [14]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": [283, 284, 285]
  }, {
    "categoryName": "Zbytek světa",
    "items": [20, 125, 286]
  }, {
    "categoryName": "Překvapení roku",
    "items": [274]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [287, 288]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [289]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "A. H. Riise Centennial Celebration 45%",
      "links": ["https://www.rumratings.com/brands/3904-a-h-riise-centennial-celebration"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": [{
      "id": null,
      "name": "Guineu Bastogne Oloroso Barrel Aged",
      "links": ["http://www.guineubeer.com/en/"]
    }]
  }]
}, {
  "id": 16,
  "name": "xenopus",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [24, 166, 200]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [290, 291, 292]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [1, 293, 294]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [295, 296, 297]
  }, {
    "categoryName": "Blended whisky",
    "items": [null, 298, 50]
  }, {
    "categoryName": "Blended malt",
    "items": [299, 288, 300]
  }, {
    "categoryName": "Grain whisky",
    "items": [301, 302, 303]
  }, {
    "categoryName": "USA a Kanada",
    "items": [304, 305, 140]
  }, {
    "categoryName": "Irsko",
    "items": [306, 307, 308]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": []
  }, {
    "categoryName": "Překvapení roku",
    "items": [{
      "id": null,
      "name": "Palírna u zeleného stromu a. s.",
      "description": "Za svoje 4 yo Reserve Žitná, Bourbon single cask 8 yo a chystané další single casks v různých typech sudů.",
      "links": []
    }]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": []
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 17,
  "name": "zombice",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [61]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [309, 310, 227]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [61, 262, 311]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [312, 265, 233]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [313, 314, 33]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [315, 70, 316]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek světa",
    "items": []
  }, {
    "categoryName": "Překvapení roku",
    "items": [317, {
      "id": null,
      "name": "palírna GlenAllachie",
      "links": []
    }, 318]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [83, 319, 169]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [320, 321, 322]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 18,
  "name": "J.K.Ch",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [323]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": [240, 324, 325]
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [326, 327, 328]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": [323, 329, 92]
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [330, 331, 332]
  }, {
    "categoryName": "Blended whisky",
    "items": [333]
  }, {
    "categoryName": "Blended malt",
    "items": [157, 37, 288]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [15, 334, 14]
  }, {
    "categoryName": "Irsko",
    "items": [335, 336, 337]
  }, {
    "categoryName": "Japonsko",
    "items": [338, 339, 340]
  }, {
    "categoryName": "Zbytek světa",
    "items": [20, 341]
  }, {
    "categoryName": "Překvapení roku",
    "items": [240]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [342]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [22, 343]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}, {
  "id": 19,
  "name": "Walter",
  "results": [{
    "categoryName": "Whisky roku – absolutní vítěz",
    "items": [344]
  }, {
    "categoryName": "Skotsko single malt 9 a méně let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 až 16 let",
    "items": [345, 346, 347]
  }, {
    "categoryName": "Skotsko single malt 17 a více let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt bez udání stáří",
    "items": [344]
  }, {
    "categoryName": "Blended whisky",
    "items": [348, 349, 249]
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": [348]
  }, {
    "categoryName": "Zbytek světa",
    "items": []
  }, {
    "categoryName": "Překvapení roku",
    "items": [349]
  }, {
    "categoryName": "Zklamání/propadák roku",
    "items": [{
      "id": null,
      "name": "Angostura 1919",
      "links": ["https://www.rumratings.com/brands/4038-angostura-1919-deluxe-aged-blend"]
    }, 350]
  }, {
    "categoryName": "Poměr cena/výkon",
    "items": [349, 345, 351]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Zacapa Centenario 23 Sistema Solera",
      "links": ["https://www.rumratings.com/brands/853-ron-zacapa-23-solera"]
    }, {
      "id": null,
      "name": "Dictador 12 yo",
      "links": ["https://www.rumratings.com/brands/304-dictador-12-year"]
    }, {
      "id": null,
      "name": "Dos Maderas PX 5 + 5",
      "links": ["https://www.rumratings.com/brands/341-dos-maderas-px-5-5"]
    }]
  }, {
    "categoryName": "Tuzemské pivo",
    "items": []
  }, {
    "categoryName": "Zahraniční pivo",
    "items": []
  }]
}];