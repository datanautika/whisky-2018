'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCssTransitionGroup = require('react-addons-css-transition-group');

var _reactAddonsCssTransitionGroup2 = _interopRequireDefault(_reactAddonsCssTransitionGroup);

var _Notifications = require('./Notifications.css');

var _Notifications2 = _interopRequireDefault(_Notifications);

var _notificationsStream = require('../streams/notificationsStream');

var _notificationsStream2 = _interopRequireDefault(_notificationsStream);

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ERROR_NOTIFICATION = _constants2.default.ERROR_NOTIFICATION;
var OK_NOTIFICATION = _constants2.default.OK_NOTIFICATION;
// const IN_PROGRESS_NOTIFICATION = constants.IN_PROGRESS_NOTIFICATION;
var NOTIFICATION_FADEOUT_DELAY = 4000;

var notificationsCount = 0;

var Notifications = function (_React$Component) {
	(0, _inherits3.default)(Notifications, _React$Component);

	function Notifications() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Notifications);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Notifications.__proto__ || (0, _getPrototypeOf2.default)(Notifications)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
			notifications: []
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Notifications, [{
		key: 'render',
		value: function render() {
			var notificationElements = [];

			this.state.notifications.forEach(function (notification) {
				notificationElements.push(_react2.default.createElement(
					'li',
					{ key: notification.id, className: _Notifications2.default.notification + (notification.type === ERROR_NOTIFICATION ? ' isError' : '') + (notification.type === OK_NOTIFICATION ? ' isSuccess' : '') },
					_react2.default.createElement(
						'p',
						{ className: _Notifications2.default.notificationTitle },
						notification.title
					),
					_react2.default.createElement(
						'p',
						{ className: _Notifications2.default.notificationText },
						notification.text
					)
				));
			});

			return _react2.default.createElement(
				'div',
				{ className: _Notifications2.default.root },
				_react2.default.createElement(
					_reactAddonsCssTransitionGroup2.default,
					{
						component: 'ul',
						transitionName: {
							enter: _Notifications2.default.notificationEnter,
							enterActive: _Notifications2.default.notificationEnterActive,
							leave: _Notifications2.default.notificationLeave,
							leaveActive: _Notifications2.default.notificationLeaveActive
						},
						transitionEnterTimeout: (_config2.default.styles.durations.faster + _config2.default.styles.durations.faster) * 1000,
						transitionLeaveTimeout: (_config2.default.styles.durations.faster + _config2.default.styles.durations.faster) * 1000 },
					notificationElements
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			_notificationsStream2.default.subscribe(function (notification) {
				var notificationId = ++notificationsCount;

				_this2.state.notifications.unshift({
					id: notificationId,
					type: notification.type,
					title: notification.title,
					text: notification.text
				});

				setTimeout(function () {
					for (var i = 0; i < _this2.state.notifications.length; i++) {
						if (_this2.state.notifications[i].id === notificationId) {
							_this2.state.notifications.splice(i, 1);

							break;
						}
					}

					_this2.forceUpdate();
				}, NOTIFICATION_FADEOUT_DELAY);

				_this2.forceUpdate();
			});
		}
	}]);
	return Notifications;
}(_react2.default.Component);

exports.default = Notifications;