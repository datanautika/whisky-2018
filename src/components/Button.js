import React from 'react';

import styles from './Button.css';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import Icon from './Icon';


const MAILTO_REGEX = /^mailto:/;
const ROUTE_LINK_REGEX = /^\//;

let browserRouter = new BrowserRouter(router);

export default class Button extends React.Component {
	shouldComponentUpdate(newProps) {
		return newProps.id !== this.props.id ||
			newProps.name !== this.props.name ||
			newProps.type !== this.props.type ||
			newProps.link !== this.props.link ||
			newProps.label !== this.props.label ||
			newProps.badge !== this.props.badge ||
			newProps.icon !== this.props.icon ||
			newProps.selectedIcon !== this.props.selectedIcon ||
			newProps.isLarge !== this.props.isLarge ||
			newProps.isDisabled !== this.props.isDisabled ||
			newProps.isSelected !== this.props.isSelected ||
			newProps.isSubmit !== this.props.isSubmit ||
			newProps.useRouter !== this.props.useRouter ||
			newProps.handleClick !== this.props.handleClick;
	}

	render() {
		let buttonClass = styles.default;

		if (this.props.type === 'flat') {
			buttonClass = styles.flat;
		} else if (this.props.type === 'invisible') {
			buttonClass = styles.invisible;
		}

		if (this.props.size === 'small') {
			buttonClass +=' isSmall';
		} else if (this.props.size === 'large') {
			buttonClass +=' isLarge';
		}

		buttonClass += this.props.isSelected ? ' isSelected' : '';
		
		buttonClass += this.props.icon || this.props.selectedIcon ? ' hasIcon' : '';
		buttonClass += (this.props.icon || this.props.selectedIcon) && !this.props.label && !this.props.badge ? ' hasOnlyIcon' : '';

		let iconElements = [];

		if (this.props.icon) {
			iconElements.push(<Icon key="icon" id={this.props.icon} />);
		}

		if (this.props.selectedIcon) {
			iconElements.push(<Icon key="selectedIcon" id={this.props.selectedIcon} />);
		}

		let buttonElement = <button
			className={buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled')}
			onClick={this.handleClick}>
			{iconElements}
			{this.props.label || null}
			{this.props.badge ? <span className={styles.badge}>{this.props.badge}</span> : null}
		</button>;

		if (this.props.isSubmit) {
			buttonElement = <input
				class={buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled')}
				type="submit"
				name={this.props.name || this.props.id || ''}
				id={this.props.id || this.props.name || ''}
				value={this.props.label || ''}
				onClick={this.handleClick}
			/>;
		} else if (this.props.link) {
			buttonElement = <a
				className={buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled')}
				href={this.props.link ? this.props.link : '#'}
				onClick={this.handleClick}>
				{iconElements}
				{this.props.label || null}
				{this.props.badge ? <span className={styles.badge}>{this.props.badge}</span> : null}
			</a>;
		}

		return buttonElement;
	}

	handleClick = (event) => {
		if (!(this.props.link || MAILTO_REGEX.test(this.props.link))) {
			event.preventDefault();

			if (this.props.handleClick) {
				this.props.handleClick();
			}
		} else if (ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false) {
			event.preventDefault();

			browserRouter.navigate(this.props.link);
		}
	};
}
