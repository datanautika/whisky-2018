import ayu from 'ayu';
import $ from 'jquery';

import router from './router';


const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

let appRoot = process.env.APP_ROOT.replace(ROUTE_STRIPPER, '');

if (appRoot.length) {
	appRoot += '(/)';
}

let routeStream = router.add(`${appRoot}(:page)(/:subpage)(/:sort)(/)`).map((value) => {
	let {page, subpage, sort} = value;
	let group = null;

	if (history.state && typeof history.state.scroll !== 'undefined') {
		requestAnimationFrame(() => {
			window.scrollTo(0, history.state.scroll);
		});
	}

	if (page !== 'oblasti' && page !== 'uzivatele' && page !== 'kategorie' && page !== 'whisky') {
		page = null;
		subpage = null;
		sort = null;
	}

	if (ayu.isFiniteLike(subpage)) {
		subpage = parseInt(subpage, 10);
	} else if (page === 'whisky') {
		group = subpage;
		subpage = null;
	}

	return {page, subpage, group, sort};
});

export default routeStream;
