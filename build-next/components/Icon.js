'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Icon = require('./Icon.css');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const OLD_EDGE_ID = 10547;
const OLD_WEBKIT_ID = 537;

function embed(svg, target) {
	// if the target exists
	if (target) {
		// create a document fragment to hold the contents of the target
		let fragment = document.createDocumentFragment();

		// cache the closest matching viewBox
		let viewBox = !svg.getAttribute('viewBox') && target.getAttribute('viewBox');

		// conditionally set the viewBox on the svg
		if (viewBox) {
			svg.setAttribute('viewBox', viewBox);
		}

		// clone the target
		let clone = target.cloneNode(true);

		// copy the contents of the clone into the fragment
		while (clone.childNodes.length) {
			fragment.appendChild(clone.firstChild);
		}

		// append the fragment into the svg
		svg.appendChild(fragment);
	}
}

function loadReadyStateChange(xhr) {
	// listen to changes in the request
	xhr.onreadystatechange = () => {
		// if the request is ready
		if (xhr.readyState === 4) {
			// get the cached html document
			let cachedDocument = xhr._cachedDocument;

			// ensure the cached html document based on the xhr response
			if (!cachedDocument) {
				cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument('');

				cachedDocument.body.innerHTML = xhr.responseText;

				xhr._cachedTarget = {};
			}

			// clear the xhr embeds list and embed each item
			xhr._embeds.splice(0).map(item => {
				// get the cached target
				let target = xhr._cachedTarget[item.id];

				// ensure the cached target
				if (!target) {
					target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id);
				}

				// embed the target into the svg
				embed(item.svg, target);
			});
		}
	};

	// test the ready state change immediately
	xhr.onreadystatechange();
}

let Icon = function (_React$Component) {
	_inherits(Icon, _React$Component);

	function Icon() {
		_classCallCheck(this, Icon);

		return _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).apply(this, arguments));
	}

	_createClass(Icon, [{
		key: 'render',
		value: function render() {
			let styleSuffix = '';
			let idSuffix = '';

			if (this.props.size === 'medium') {
				styleSuffix = '--medium';
				idSuffix = '-medium';
			} else if (this.props.size === 'large') {
				styleSuffix = '--large';
				idSuffix = '-large';
			}

			return _react2.default.createElement(
				'span',
				{ ref: 'root', className: _Icon2.default['root' + styleSuffix] },
				_react2.default.createElement(
					'svg',
					{ xmlns: 'http://www.w3.org/2000/svg' },
					_react2.default.createElement('use', { ref: 'use', xlinkHref: `./assets/icons.svg#${this.props.id + idSuffix}` })
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			let domNode = this.refs.root;

			if (!domNode) {
				return;
			}

			let useNode = this.refs.use;

			// set whether the polyfill will be activated or not
			let polyfill;
			let newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/;
			let webkitUA = /\bAppleWebKit\/(\d+)\b/;
			let olderEdgeUA = /\bEdge\/12\.(\d+)\b/;

			polyfill = newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < OLD_EDGE_ID || (navigator.userAgent.match(webkitUA) || [])[1] < OLD_WEBKIT_ID;

			// create xhr requests object
			let requests = {};

			if (polyfill) {
				requestAnimationFrame(() => {
					// get the current <svg>
					let svg = useNode.parentNode;

					if (svg && /svg/i.test(svg.nodeName)) {
						let src = useNode.getAttribute('xlink:href') || useNode.getAttribute('href');

						// remove the <use> element
						svg.removeChild(useNode);

						// parse the src and get the url and id
						let srcSplit = src.split('#');
						let url = srcSplit.shift();
						let id = srcSplit.join('#');

						// if the link is external
						if (url.length) {
							// get the cached xhr request
							let xhr = requests[url];

							// ensure the xhr request exists
							if (!xhr) {
								xhr = requests[url] = new XMLHttpRequest();

								xhr.open('GET', url);
								xhr.send();

								xhr._embeds = [];
							}

							// add the svg and id as an item to the xhr embeds list
							xhr._embeds.push({
								svg,
								id
							});

							// prepare the xhr ready state change event
							loadReadyStateChange(xhr);
						} else {
							// embed the local id into the svg
							embed(svg, document.getElementById(id));
						}
					}
				});
			}
		}
	}]);

	return Icon;
}(_react2.default.Component);

exports.default = Icon;