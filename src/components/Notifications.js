import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import styles from './Notifications.css';
import notificationsStream from '../streams/notificationsStream';
import constants from '../internals/constants';
import config from '../config';


const ERROR_NOTIFICATION = constants.ERROR_NOTIFICATION;
const OK_NOTIFICATION = constants.OK_NOTIFICATION;
// const IN_PROGRESS_NOTIFICATION = constants.IN_PROGRESS_NOTIFICATION;
const NOTIFICATION_FADEOUT_DELAY = 4000;

let notificationsCount = 0;

export default class Notifications extends React.Component {
	state = {
		notifications: []
	};

	render() {
		let notificationElements = [];

		this.state.notifications.forEach((notification) => {
			notificationElements.push(<li key={notification.id} className={styles.notification + (notification.type === ERROR_NOTIFICATION ? ' isError' : '') + (notification.type === OK_NOTIFICATION ? ' isSuccess' : '')}>
				<p className={styles.notificationTitle}>{notification.title}</p>
				<p className={styles.notificationText}>{notification.text}</p>
			</li>);
		});

		return <div className={styles.root}>
			<ReactCSSTransitionGroup
				component="ul"
				transitionName={{
					enter: styles.notificationEnter,
					enterActive: styles.notificationEnterActive,
					leave: styles.notificationLeave,
					leaveActive: styles.notificationLeaveActive
				}}
				transitionEnterTimeout={(config.styles.durations.faster + config.styles.durations.faster) * 1000}
				transitionLeaveTimeout={(config.styles.durations.faster + config.styles.durations.faster) * 1000}>
				{notificationElements}
			</ReactCSSTransitionGroup>
		</div>;
	}


	componentDidMount() {
		notificationsStream.subscribe((notification) => {
			let notificationId = ++notificationsCount;

			this.state.notifications.unshift({
				id: notificationId,
				type: notification.type,
				title: notification.title,
				text: notification.text
			});

			setTimeout(() => {
				for (let i = 0; i < this.state.notifications.length; i++) {
					if (this.state.notifications[i].id === notificationId) {
						this.state.notifications.splice(i, 1);

						break;
					}
				}

				this.forceUpdate();
			}, NOTIFICATION_FADEOUT_DELAY);

			this.forceUpdate();
		});
	}
}
