'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// regex for stripping a leading hash/slash and trailing space.
var ROUTE_STRIPPER = /^[#\/]|\s+$/g;

// regexes for matching named parameter parts and splatted parts of route strings.
var OPTIONAL_PARAM = /\((.*?)\)/g;
var NAMED_PARAM = /(\(\?)?:\w+/g;
var SPLAT_PARAM = /\*\w+/g;
var ESCAPE_REGEX = /[\-{}\[\]+?.,\\\^$|#\s]/g;

/**
 * Convert a route string into a regular expression, suitable for matching against the current location hash.
 *
 * @param {string} route
 * @returns {RegExp}
 */
function routeToRegExp(route) {
	var parameterNames = [];
	var routeRegExp = new RegExp('^' + route.replace(ESCAPE_REGEX, '\\$&').replace(OPTIONAL_PARAM, '(?:$1)?').replace(NAMED_PARAM, function (match, optional) {
		parameterNames.push(match.slice(1));

		return optional ? match : '([^/?]+)';
	}).replace(SPLAT_PARAM, '([^?]*?)') + '(?:\\?([\\s\\S]*))?$');

	routeRegExp.parameterNames = parameterNames;

	return routeRegExp;
}

/**
 * Given a route, and a URL fragment that it matches, return the array of extracted decoded parameters. Empty or unmatched parameters will be treated as `null` to normalize cross-browser behavior.
 *
 * @param {RegExp} routeRegExp
 * @param {string} fragment
 * @returns {Array}
 */
function extractParameters(routeRegExp, fragment) {
	var parameters = routeRegExp.exec(fragment);

	if (parameters) {
		parameters = parameters.slice(1);

		return parameters.map(function (parameter, index) {
			if (parameters && index === parameters.length - 1) {
				// don't decode the search parameters
				return parameter || null;
			}

			return parameter ? decodeURIComponent(parameter) : null;
		});
	}

	return null;
}

/**
 * Normalizes path fragment by stripping a leading hash/slash and trailing space.
 *
 * @param {string} fragment
 * @returns {string}
 */
function normalizePathFragment(fragment) {
	return fragment.replace(ROUTE_STRIPPER, '');
}

/**
 * Router class.
 */

var Router = function () {
	function Router() {
		(0, _classCallCheck3.default)(this, Router);
		this.routes = [];
	}

	(0, _createClass3.default)(Router, [{
		key: 'trigger',
		value: function trigger() {
			var fragment = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
			var context = arguments[1];

			var normalizedFragment = normalizePathFragment(fragment);

			for (var i = 0; i < this.routes.length; i++) {
				if (this.routes[i].route.test(normalizedFragment)) {
					var parameterNames = this.routes[i].route.parameterNames.concat('search');
					var parameters = extractParameters(this.routes[i].route, normalizedFragment);
					var result = {};

					if (parameters) {
						for (var j = 0; j < parameterNames.length; j++) {
							result[parameterNames[j]] = parameters[j];
						}

						if (context) {
							result.context = context;
						}

						this.routes[i].stream.push(result);

						return true;
					}
				}
			}

			return false;
		}

		/**
   * Creates route.
   *
   * @param {string} route
   * @param {?Stream} stream
   * @returns {Stream}
   */

	}, {
		key: 'add',
		value: function add(route) {
			var stream = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : new _Stream2.default();

			this.routes.unshift({
				route: routeToRegExp(route),
				stream: stream instanceof _Stream2.default ? stream : new _Stream2.default()
			});

			return this.routes[0].stream;
		}
	}]);
	return Router;
}();

exports.default = Router;