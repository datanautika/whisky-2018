import React from 'react';

import styles from './Input.css';


const ENTER_KEY_CODE = 13;

export default class Input extends React.Component {
	shouldComponentUpdate(newProps) {
		return newProps.id !== this.props.id ||
			newProps.name !== this.props.name ||
			newProps.type !== this.props.type ||
			newProps.autocomplete !== this.props.autocomplete ||
			newProps.isValid !== this.props.isValid ||
			newProps.isInvalid !== this.props.isInvalid ||
			newProps.isDisabled !== this.props.isDisabled ||
			newProps.handleChange !== this.props.handleChange ||
			newProps.handleSave !== this.props.handleSave ||
			newProps.validator !== this.props.validator;
	}

	render() {
		let inputProps = {
			key: this.props.id || this.props.name || '',
			className: styles.default + (this.props.isValid ? ' isValid' : '') + (this.props.isInvalid ? ' isInvalid' : '') + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
			type: 'text',
			name: this.props.name || this.props.id || '',
			id: this.props.id || this.props.name || '',
			onBlur: this.handleFocusOut,
			onChange: this.handleInput,
			onIeydown: this.handleKeyDown
		};

		if (this.props.type === 'email') {
			inputProps.type = this.props.type;
		}

		if (this.props.isDisabled) {
			inputProps.disabled = 'disabled';
		}

		if (this.props.autocomplete === false) {
			inputProps.autocomplete = 'off';
		}

		return <input {...inputProps} />;
	}

	handleInput(event) {
		if (this.props.handleChange) {
			this.props.handleChange(this.validate(event.target.value));
		}
	}

	handleFocusOut(event) {
		if (this.props.handleSave) {
			this.props.handleSave(this.validate(event.target.value));
		}
	}

	handleKeyDown(event) {
		if (event.keyCode === ENTER_KEY_CODE) {
			if (this.props.handleSave) {
				this.props.handleSave(this.validate(event.target.value));
			}
		}
	}

	validate(value) {
		return this.props.validator ? this.props.validator(value) : value;
	}
}
