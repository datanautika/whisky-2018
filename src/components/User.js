import React from 'react';
import _ from 'lodash';

import styles from './User.css';
import WhiskyName from './WhiskyName';
import href from '../utils/href';
import {users, whiskies, categories} from '../data';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';


const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 2, maximumFractionDigits: 2});
const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

export default class User extends React.Component {
	render() {
		let user = this.props.data;

		let categoriesElements = user.results.map((result, resultIndex) => {
			let itemsElements = result.items.map((item, itemIndex) => {
				if (item && typeof item === 'number') {
					return <li key={itemIndex} className={styles.categoryItem}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item})} /></li>;
				} else if (item && typeof item === 'object' && typeof item.id === 'number') {
					return <li key={itemIndex} className={styles.categoryItem}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item.id})} /><p className={styles.categoryItemDescription}>{item.description}</p></li>;
				} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
					return <li key={itemIndex} className={styles.categoryItem}>
						<p className={styles.categoryItemName}>{item.name}</p>
						<p className={styles.categoryItemDescription}>{item.description}</p>
						{item.links && item.links.length ? <ul className={styles.categoryItemLinks}>{item.links.map((link, index) => <li key={index}><a href={link}>{link}</a></li>)}</ul> : null}
					</li>;
				}

				return <li key={itemIndex} className={`${styles.categoryItem} ${styles.isEmpty}`}><p>–</p></li>;
			});

			return <section className={styles.category} key={resultIndex} onClick={this.handleClick}>
				<h3 className={styles.categoryHeading}><a href={href('kategorie', _.find(categories, {name: result.categoryName}).id)}>{result.categoryName}</a></h3>
				{itemsElements && itemsElements.length ? <ol className={styles.categoryResults}>{itemsElements}</ol> : <p>–</p>}
			</section>;
		});

		let tempUsers = [];

		users.forEach((otherUser, otherUserIndex) => {
			if (otherUser !== user) {
				tempUsers.push({
					id: otherUser.id,
					name: otherUser.name,
					similarity: user.similarities[otherUserIndex]
				});
			}
		});

		tempUsers.sort((tempUser1, tempUser2) => tempUser2.similarity - tempUser1.similarity);

		let usersElements = user.similarities.map((similarity) => similarity.userId === user.id ? null : <li className={styles.result} key={similarity.userId}>
			<div className={styles.resultHeader}>
				<h4 className={styles.resultHeading}>
					<span className={styles.resultHeadingLabel}>Uživatel</span>
					<a href={href('uzivatele', similarity.userName)}>{`${similarity.userName}`}</a>
				</h4>

				<div className={styles.points}>
					<span className={`${styles.resultHeadingLabel} ${styles.isRightAligned}`}>Kosinová podobnost × 100</span>
					<span className={styles.pointsCount}>{`${NUMBER_FORMAT.format(similarity.cosineSimilarity * 100)}`}</span>
				</div>

				{/*<div className={styles.points}>
					<span className={`${styles.resultHeadingLabel} ${styles.isRightAligned}`}>Euklidovská vzdálenost</span>
					<span className={styles.pointsCount}>{`${NUMBER_FORMAT.format(similarity.euclideanDistance)}`}</span>
				</div>*/}
			</div>
		</li>);

		return <div className={styles.root}>
			<h2 className={styles.nameHeading}>{`${user.name}`}</h2>
			{categoriesElements}

			<section>
				<h3 className={styles.categoryHeading}>Nejpodobněji hlasovali</h3>
				<ol>
					{usersElements}
				</ol>
			</section>
		</div>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
