'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('./Button.css');

var _Button2 = _interopRequireDefault(_Button);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const MAILTO_REGEX = /^mailto:/;
const ROUTE_LINK_REGEX = /^\//;

let browserRouter = new _BrowserRouter2.default(_router2.default);

let Button = function (_React$Component) {
	_inherits(Button, _React$Component);

	function Button() {
		var _temp, _this, _ret;

		_classCallCheck(this, Button);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).call(this, ...args)), _this), _this.handleClick = event => {
			if (!(_this.props.link || MAILTO_REGEX.test(_this.props.link))) {
				event.preventDefault();

				if (_this.props.handleClick) {
					_this.props.handleClick();
				}
			} else if (ROUTE_LINK_REGEX.test(_this.props.link) && _this.props.useRouter !== false) {
				event.preventDefault();

				browserRouter.navigate(_this.props.link);
			}
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Button, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			return newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.type !== this.props.type || newProps.link !== this.props.link || newProps.label !== this.props.label || newProps.badge !== this.props.badge || newProps.icon !== this.props.icon || newProps.selectedIcon !== this.props.selectedIcon || newProps.isLarge !== this.props.isLarge || newProps.isDisabled !== this.props.isDisabled || newProps.isSelected !== this.props.isSelected || newProps.isSubmit !== this.props.isSubmit || newProps.useRouter !== this.props.useRouter || newProps.handleClick !== this.props.handleClick;
		}
	}, {
		key: 'render',
		value: function render() {
			let buttonClass = _Button2.default.default;

			if (this.props.type === 'flat') {
				buttonClass = _Button2.default.flat;
			} else if (this.props.type === 'invisible') {
				buttonClass = _Button2.default.invisible;
			}

			if (this.props.size === 'small') {
				buttonClass += ' isSmall';
			} else if (this.props.size === 'large') {
				buttonClass += ' isLarge';
			}

			buttonClass += this.props.isSelected ? ' isSelected' : '';

			buttonClass += this.props.icon || this.props.selectedIcon ? ' hasIcon' : '';
			buttonClass += (this.props.icon || this.props.selectedIcon) && !this.props.label && !this.props.badge ? ' hasOnlyIcon' : '';

			let iconElements = [];

			if (this.props.icon) {
				iconElements.push(_react2.default.createElement(_Icon2.default, { key: 'icon', id: this.props.icon }));
			}

			if (this.props.selectedIcon) {
				iconElements.push(_react2.default.createElement(_Icon2.default, { key: 'selectedIcon', id: this.props.selectedIcon }));
			}

			let buttonElement = _react2.default.createElement(
				'button',
				{
					className: buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
					onClick: this.handleClick },
				iconElements,
				this.props.label || null,
				this.props.badge ? _react2.default.createElement(
					'span',
					{ className: _Button2.default.badge },
					this.props.badge
				) : null
			);

			if (this.props.isSubmit) {
				buttonElement = _react2.default.createElement('input', {
					'class': buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
					type: 'submit',
					name: this.props.name || this.props.id || '',
					id: this.props.id || this.props.name || '',
					value: this.props.label || '',
					onClick: this.handleClick
				});
			} else if (this.props.link) {
				buttonElement = _react2.default.createElement(
					'a',
					{
						className: buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
						href: this.props.link ? this.props.link : '#',
						onClick: this.handleClick },
					iconElements,
					this.props.label || null,
					this.props.badge ? _react2.default.createElement(
						'span',
						{ className: _Button2.default.badge },
						this.props.badge
					) : null
				);
			}

			return buttonElement;
		}
	}]);

	return Button;
}(_react2.default.Component);

exports.default = Button;