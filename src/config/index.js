import styles from './styles';
import i18nStrings from './i18nStrings';


export default {
	styles,
	i18nStrings
};
