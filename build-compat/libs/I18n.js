'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// regex that matches optional type annotations in i18n strings, e.g. i18n `This is a number ${x}:n(2)` formats x as number with two fractional digits
var TYPE_REGEX = /^:([a-z])(\((.+)\))?/;

/**
 * e.g. buildKey(['', ' has ', ':c in the']) == '{0} has {1} in the bank'
 *
 * @param {Array} literals
 * @returns {string}
 */
var buildKey = function buildKey(literals) {
	var stripType = function stripType(s) {
		return s.replace(TYPE_REGEX, '');
	};
	var lastPartialKey = stripType(literals[literals.length - 1]);
	var prependPartialKey = function prependPartialKey(memo, curr, i) {
		return stripType(curr) + '{' + i + '}' + memo;
	};

	return literals.slice(0, -1).reduceRight(prependPartialKey, lastPartialKey);
};

/**
 * e.g. formatStrings('{0} {1}!', 'hello', 'world') == 'hello world!'
 *
 * @param {[type]} string
 * @param {...*} values
 * @returns {[type]}
 */
var buildMessage = function buildMessage(string) {
	for (var _len = arguments.length, values = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
		values[_key - 1] = arguments[_key];
	}

	return string.replace(/{(\d)}/g, function (_, index) {
		return values[Number(index)];
	});
};

/**
 * Localizes general string.
 *
 * @param {string} locale
 * @param {string} string
 * @returns {string}
 */
var localizeString = function localizeString(locale, string) {
	return string.toLocaleString(locale);
};

/**
 * Localizes currency string.
 *
 * @param {string} locale
 * @param {string} string
 * @param {string} currency
 * @returns {string}
 */
var localizeCurrency = function localizeCurrency(locale, string, currency) {
	return string.toLocaleString(locale, {
		style: 'currency',
		currency: currency
	});
};

/**
 * Localizes number string.
 *
 * @param {string} locale
 * @param {string} string
 * @param {number} fractionalDigits
 * @returns {string}
 */
var localizeNumber = function localizeNumber(locale, string, fractionalDigits) {
	return string.toLocaleString(locale, {
		minimumFractionDigits: fractionalDigits,
		maximumFractionDigits: fractionalDigits
	});
};

/**
 * Extracts type info from a string.
 *
 * @param {string} literal
 * @returns {Object}
 */
var extractTypeInfo = function extractTypeInfo(literal) {
	var match = TYPE_REGEX.exec(literal);

	if (match) {
		return { type: match[1], options: match[3] };
	}

	return { type: 's', options: '' };
};

/**
 * Localizes string.
 *
 * @param {string} value
 * @param {I18n} i18n
 * @param {string} options.type
 * @param {*} options.options
 * @returns {[type]}
 */
var localize = function localize(value, i18n, _ref) {
	var type = _ref.type,
	    options = _ref.options;

	var localizedValue = void 0;

	if (type === 's') {
		localizedValue = localizeString(i18n.locale, value, options);
	}

	if (type === 'c') {
		localizedValue = localizeCurrency(i18n.locale, value, options || i18n.currency);
	}

	if (type === 'n') {
		localizedValue = localizeNumber(i18n.locale, value, options);
	}

	return localizedValue;
};

var i18n = void 0;

/**
 * I18n class.
 * Singleton.
 */

var I18n = function () {

	/**
  * Creates an I18n instance.
  *
  * @returns {I18n}
  */
	function I18n() {
		(0, _classCallCheck3.default)(this, I18n);
		this.strings = {};
		this.currency = '';
		this.locale = '';

		i18n = i18n ? i18n : this;

		return i18n;
	}

	/**
  * Changes localization options.
  *
  * @param {Object} options
  * @param {Object} options.strings
  * @param {string} options.currency
  * @param {string} options.locale
  * @returns {this}
  */


	(0, _createClass3.default)(I18n, [{
		key: 'use',
		value: function use() {
			var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
			    _ref2$strings = _ref2.strings,
			    strings = _ref2$strings === undefined ? {} : _ref2$strings,
			    _ref2$currency = _ref2.currency,
			    currency = _ref2$currency === undefined ? '$' : _ref2$currency,
			    _ref2$locale = _ref2.locale,
			    locale = _ref2$locale === undefined ? 'en-US' : _ref2$locale;

			this.strings = strings;
			this.currency = currency;
			this.locale = locale;

			return this;
		}

		/**
   * Tag function for template string. Uses i18n instance localization options for translation.
   *
   * @param {Array<string>} literals
   * @param {...*} values
   * @returns {string}
   */

	}, {
		key: 'translate',
		value: function translate(literals) {
			var _this = this;

			var translationKey = buildKey(literals);
			var translationString = void 0;

			if (this.strings[translationKey]) {
				translationString = this.strings[translationKey][this.locale];
			}

			if (!translationString) {
				translationString = translationKey;
			}

			if (translationString) {
				var typeInfoForValues = literals.slice(1).map(extractTypeInfo);

				for (var _len2 = arguments.length, values = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
					values[_key2 - 1] = arguments[_key2];
				}

				var localizedValues = values.map(function (value, index) {
					return localize(value, _this, typeInfoForValues[index]);
				});

				return buildMessage.apply(undefined, [translationString].concat((0, _toConsumableArray3.default)(localizedValues)));
			}

			return 'Error: translation missing!';
		}
	}]);
	return I18n;
}();

exports.default = I18n;