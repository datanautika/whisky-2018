import React from 'react';
import _ from 'lodash';
import ayu from 'ayu';

import styles from './WhiskyList.css';
import WhiskyName from './WhiskyName';
import {votes, categories} from '../data';
import whiskyName from '../utils/whiskyName';
import href from '../utils/href';
import routeStream from '../streams/routeStream';


const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 2, maximumFractionDigits: 2});
const WHOLE_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 0, maximumFractionDigits: 0});

let cache = {};

export default class WhiskyList extends React.Component {
	render() {
		let routeStreamValue = routeStream.get();
		let group = '';
		let sort = 'name';
		let groupName;

		if (routeStreamValue.group === 'kategorie') {
			group = 'category';
			groupName = 'Kategorie';
		}

		if (routeStreamValue.group === 'oblast') {
			group = 'region';
			groupName = 'Oblast';
		}

		if (routeStreamValue.group === 'palirna') {
			group = 'distillery';
			groupName = 'Palírna';
		}

		if (routeStreamValue.sort === 'nazev') {
			sort = 'name';
		}

		if (routeStreamValue.sort === 'pocet-bodu') {
			sort = 'points-sum';
		}

		if (routeStreamValue.sort === 'prumerny-pocet-bodu') {
			sort = 'points-mean';
		}

		if (routeStreamValue.sort === 'upraveny-pocet-bodu') {
			sort = 'adjusted-points-sum';
		}

		if (routeStreamValue.sort === 'pocet-lahvi') {
			sort = 'whiskies-sum';
		}

		if ((sort !== 'name' && sort !== 'points-sum' && sort !== 'points-mean' && sort !== 'adjusted-points-sum' && sort !== 'whiskies-sum') || (group !== 'region' && sort === 'adjusted-points-sum') || (!group && sort !== 'points-sum')) {
			sort = 'name';
		}

		let cacheId = `${group}:${sort}`;
		let results;

		if (cache[cacheId]) {
			results = cache[cacheId];
		} else {
			let model = {
				voteIds: new ayu.Dimension({
					variable: 'id',
					scale: ayu.NOMINAL_SCALE
				})
			};

			if (group) {
				model[group] = new ayu.Dimension({
					variable: group === 'category' ? 'categoryName' : group,
					scale: ayu.NOMINAL_SCALE,
					group: true
				});
			}

			results = new ayu.DataModel(model, votes);

			if (group) {
				results.filter((value) => {
					if (group === 'distillery' && value.groups[group]) {
						return value.groups[group].split(/(\s*,\s*)|(\s&\s)/).length === 1;
					}

					return value.groups[group];
				});
			}

			results.map((value) => {
				let newValue = {
					groups: _.cloneDeep(value.groups)
				};

				if (group) {
					newValue.whiskies = value.voteIds.map((voteId) => _.cloneDeep(_.find(votes, {id: voteId})));
				} else {
					newValue.whisky = _.cloneDeep(_.find(votes, {id: value.voteIds}));
				}

				if (group) {
					newValue[group] = newValue.groups[group];
					newValue.pointsSum = newValue.whiskies[0].pointsCount;
					newValue.whiskies[0].id = newValue.whiskies[0].whiskyId;

					delete newValue.whiskies[0].whiskyId;

					for (let i = 1; i < newValue.whiskies.length; i++) {
						let deleteWhisky = false;

						newValue.pointsSum += newValue.whiskies[i].pointsCount;
						newValue.whiskies[i].id = newValue.whiskies[i].whiskyId;

						delete newValue.whiskies[i].whiskyId;

						for (let j = 0; j < i; j++) {
							if (newValue.whiskies[j].id === newValue.whiskies[i].id) {
								newValue.whiskies[j].pointsCount += newValue.whiskies[i].pointsCount;
								deleteWhisky = true;

								break;
							}
						}

						if (deleteWhisky) {
							newValue.whiskies.splice(i, 1);

							i--;
						}
					}

					newValue.whiskiesCount = newValue.whiskies.length;
					newValue.pointsMean = newValue.pointsSum / newValue.whiskiesCount;

					if (group === 'region') {
						newValue.adjustedPointsSum = newValue.pointsSum;

						if (value.groups.region === 'Speyside') {
							newValue.adjustedPointsSum /= 48;
							newValue.region = 'Speyside';
						}

						if (value.groups.region === 'Highlands') {
							newValue.adjustedPointsSum /= 34;
							newValue.region = 'Vysočina';
						}

						if (value.groups.region === 'Lowlands') {
							newValue.adjustedPointsSum /= 9;
							newValue.region = 'Nížina';
						}

						if (value.groups.region === 'Islay') {
							newValue.adjustedPointsSum /= 8;
							newValue.region = 'Islay';
						}

						if (value.groups.region === 'Islands') {
							newValue.adjustedPointsSum /= 9;
							newValue.region = 'Ostrovy';
						}

						if (value.groups.region === 'Campbeltown') {
							newValue.adjustedPointsSum /= 3;
							newValue.region = 'Campbeltown';
						}
					}

					newValue.whiskies.sort((a, b) => {
						let result = b.pointsCount - a.pointsCount;

						if (result !== 0) {
							return result;
						}

						return whiskyName(a).join(' ').trim().localeCompare(whiskyName(b).join(' ').trim());
					});
				}

				return newValue;
			});

			// results are not grouped, but we still have to combine whiskies
			if (!group) {
				results.values[0].pointsSum = results.values[0].whisky.pointsCount;
				results.values[0].whisky.id = results.values[0].whisky.whiskyId;

				delete results.values[0].whisky.whiskyId;

				for (let i = 1; i < results.values.length; i++) {
					let deleteWhisky = false;

					results.values[i].pointsSum = results.values[i].whisky.pointsCount;
					results.values[i].whisky.id = results.values[i].whisky.whiskyId;

					delete results.values[i].whisky.whiskyId;

					for (let j = 0; j < i; j++) {
						if (results.values[j].whisky.id === results.values[i].whisky.id) {
							results.values[j].pointsSum += results.values[i].whisky.pointsCount;
							deleteWhisky = true;

							break;
						}
					}

					if (deleteWhisky) {
						results.values.splice(i, 1);

						i--;
					}
				}
			}

			if (sort) {
				results.sort((a, b) => {
					if (group) {
						let variable = 'pointsSum';

						if (sort === 'name') {
							variable = group;

							return a[variable].localeCompare(b[variable]);
						}

						if (sort === 'points-mean') {
							variable = 'pointsMean';
						}

						if (sort === 'adjusted-points-sum') {
							variable = 'adjustedPointsSum';
						}

						if (sort === 'whiskies-sum') {
							variable = 'whiskiesCount';
						}

						return b[variable] - a[variable];
					}

					if (sort === 'name') {
						let [titlePart1A, titlePart2A, titlePart3A, subtitleA] = whiskyName(a.whisky);
						let [titlePart1B, titlePart2B, titlePart3B, subtitleB] = whiskyName(b.whisky);

						if (!titlePart1A) {
							titlePart1A = titlePart2A;
						}

						if (!titlePart1B) {
							titlePart1B = titlePart2B;
						}

						let result = titlePart1A.trim().localeCompare(titlePart1B.trim());

						if (result !== 0) {
							return result;
						}

						if (a.whisky.age && b.whisky.age) {
							return a.whisky.age - b.whisky.age;
						}

						return `${titlePart2A} ${titlePart3A} ${subtitleA}`.trim().localeCompare(`${titlePart2B} ${titlePart3B} ${subtitleB}`.trim());
					}

					return b.pointsSum - a.pointsSum;
				});
			}

			cache[cacheId] = results;
		}

		return <div className={styles.root}>
			<h2 className={styles.heading}>Whisky</h2>

			<div className={styles.filters}>
				<div className={styles.filter}>
					<h4 className={styles.filtersHeading}>Roztřídit podle</h4>
					<ol className={styles.filterList} onClick={this.handleGroupFilterClick}>
						<li><a className={group === 'category' ? styles.isSelected : ''} data-group="category" href={href('whisky', 'kategorie', routeStreamValue.sort)}>Kategorie</a></li>
						<li><a className={group === 'region' ? styles.isSelected : ''} data-group="region" href={href('whisky', 'oblast', routeStreamValue.sort)}>Oblasti</a></li>
						<li><a className={group === 'distillery' ? styles.isSelected : ''} data-group="distillery" href={href('whisky', 'palirna', routeStreamValue.sort)}>Palírny</a></li>
						<li><a data-group="distillery" href={href('whisky', '-', routeStreamValue.sort)}>(zrušit)</a></li>
					</ol>
				</div>

				<div className={styles.filter}>
					<h4 className={styles.filtersHeading}>Seřadit podle</h4>
					<ol className={styles.filterList} onClick={this.handleSortFilterClick}>
						<li><a className={sort === 'name' ? styles.isSelected : ''} data-sort="name" href={href('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'nazev')}>Názvu</a></li>
						{group ? <li><a className={sort === 'whiskies-sum' ? styles.isSelected : ''} data-sort="whiskies-sum" href={href('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'pocet-lahvi')}>Počtu lahví</a></li> : null}
						<li><a className={sort === 'points-sum' ? styles.isSelected : ''} data-sort="points-sum" href={href('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'pocet-bodu')}>Počtu bodů</a></li>
						{group === 'region' ? <li><a className={sort === 'adjusted-points-sum' ? styles.isSelected : ''} data-sort="adjusted-points-sum" href={href('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'upraveny-pocet-bodu')}>Upraveného počtu bodů</a></li> : null}
						{group ? <li><a className={sort === 'points-mean' ? styles.isSelected : ''} data-sort="points-mean" href={href('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'prumerny-pocet-bodu')}>Průměrného počtu bodů</a></li> : null}

					</ol>
				</div>
			</div>

			{group ? results.values.map((value, valueIndex) => <section className={styles.result} key={valueIndex}>
				<header className={styles.resultHeader}>
					<h3 className={styles.resultHeading}>
						<span className={styles.resultHeadingLabel}>{groupName}</span>
						{group === 'category' ? <a href={href('kategorie', _.find(categories, {name: value[group]}).id)}>{value[group]}</a> : value[group]}
					</h3>

					<div className={styles.points}>
						<span className={styles.resultHeadingLabel}>Počet lahví</span>
						<span className={styles.pointsCount}>{WHOLE_NUMBER_FORMAT.format(value.whiskiesCount)}</span>
					</div>

					<div className={styles.points}>
						<span className={styles.resultHeadingLabel}>Počet bodů</span>
						<span className={styles.pointsCount}>{WHOLE_NUMBER_FORMAT.format(value.pointsSum)}</span>
					</div>

					{typeof value.adjustedPointsSum === 'undefined' ? null : <div className={styles.points}>
						<span className={styles.resultHeadingLabel}>Upravený počet bodů</span>
						<span className={styles.pointsCount}>{NUMBER_FORMAT.format(value.adjustedPointsSum)}</span>
					</div>}

					<div className={styles.points}>
						<span className={styles.resultHeadingLabel}>Průměrný počet bodů</span>
						<span className={styles.pointsCount}>{NUMBER_FORMAT.format(value.pointsMean)}</span>
					</div>
				</header>

				<ol>
					{value.whiskies.map((whisky) => <li key={whisky.id} className={styles.whisky}>
						<WhiskyName data={whisky} />
						{/*whisky.pointsCount ? <span className={styles.pointsCount}>{`${whisky.pointsCount} `}<span className={styles.pointsCountLabel}>{((pointsCount) => {
							if (pointsCount === 1) {
								return 'bod';
							}

							if (pointsCount === 2 || pointsCount === 3 || pointsCount === 4) {
								return 'body';
							}

							return 'bodů';
						})(whisky.pointsCount)}</span></span> : null*/whisky.pointsCount ? <span className={styles.pointsCount}>{whisky.pointsCount}</span> : null}
					</li>)}
				</ol>
			</section>) : <ol>
				{results.values.map((value) => <li key={value.whisky.id} className={styles.whisky}>
					<WhiskyName data={value.whisky} />
					{/*value.pointsSum ? <span className={styles.pointsCount}>{`${value.pointsSum} `}<span className={styles.pointsCountLabel}>{((pointsSum) => {
						if (pointsSum === 1) {
							return 'bod';
						}

						if (pointsSum === 2 || pointsSum === 3 || pointsSum === 4) {
							return 'body';
						}

						return 'bodů';
					})(value.pointsSum)}</span></span> : null*/value.pointsSum ? <span className={styles.pointsCount}>{value.pointsSum}</span> : null}
				</li>)}
			</ol>}
		</div>;
	}
}
