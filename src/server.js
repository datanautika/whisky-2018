import './internals/polyfills';

import Koa from 'koa';
import compress from 'koa-compress';
import logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import fs from 'fs';
import path from 'path';
import http from 'http';
import serve from 'koa-static';

import appRoot from './internals/appRoot';


const HTTP_NOT_FOUND = 404;
const HTTP_INTERNAL_ERROR = 500;
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

let readFile = Promise.promisify(fs.readFile);
let app = new Koa();
let uriAppRoot = process.env.APP_ROOT.replace(ROUTE_STRIPPER, '');

console.log('App root dir:', `"${appRoot}"`); // eslint-disable-line no-console
console.log('App root URL:', `"${uriAppRoot}"`); // eslint-disable-line no-console

app.use(bodyParser());
app.use(logger());
app.use(async (context, next) => {
	try {
		await next();
	} catch (error) {
		console.warn('\nError!\n', error); // eslint-disable-line no-console

		context.status = error.status || HTTP_INTERNAL_ERROR;

		context.body = {
			error: {
				message: error.message
			}
		};

		context.app.emit('error', error, context);
	}
});
app.use(serve(path.resolve(path.join(appRoot, 'public'))));

// serve app
app.use(async (context, next) => {
	if (context.method !== 'HEAD' && context.method !== 'GET') {
		await next();

		return;
	}

	// response is already handled
	if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
		await next();

		return;
	}

	context.body = await readFile(path.join(appRoot, 'public/index.html'), 'utf8');

	await next();
});

// compression
app.use(compress());

const PORT = 8080;

http.createServer(app.callback()).listen(process.env.PORT || PORT);

console.log(`Listening on port ${process.env.PORT || PORT}...`); // eslint-disable-line no-console
