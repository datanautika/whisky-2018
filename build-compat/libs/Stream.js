'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _isFunction = require('../internals/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var trueFn = function trueFn() {
	return true;
};
var streamsToUpdate = [];
var inStream = void 0;
var streamOrder = [];
var nextStreamOrderIndex = -1;
var isFlushing = false;

var Stream = function () {

	/**
  * Creates a stream, with initial value of `value`.
  *
  * @param {?V} value
  * @returns {Stream}
  */
	// TODO: generic for stream function
	function Stream(value) {
		(0, _classCallCheck3.default)(this, Stream);

		_initialiseProps.call(this);

		if (value === trueFn) {
			this.fn = value;
			this.isEndStream = true;
			this.end = undefined;
		} else {
			this.end = new Stream(trueFn);

			this.end.listeners.push(this);

			if (arguments.length > 0 && typeof value !== 'undefined') {
				this.push(value);
			}
		}

		this.push = this.push.bind(this);
	}

	(0, _createClass3.default)(Stream, [{
		key: 'updateStream',
		value: function updateStream() {
			if (this.areDependenciesMet !== true && !this.dependencies.every(function (stream) {
				return stream.hasValue;
			}) || this.end !== undefined && this.end.value === true) {
				return this;
			}

			if (inStream !== undefined) {
				streamsToUpdate.push(this);

				return this;
			}

			inStream = this;

			var newValue = void 0;

			if (this.fn) {
				newValue = this.fn.apply(this, [this, this.changedDependencies].concat((0, _toConsumableArray3.default)(this.dependencies)));
			}

			if (newValue !== undefined) {
				this.push(newValue);
			}

			inStream = undefined;
			this.changedDependencies = [];
			this.shouldUpdate = false;

			if (isFlushing === false) {
				Stream.flushUpdate();
			}

			return this;
		}
	}, {
		key: 'updateDependencies',
		value: function updateDependencies() {
			for (var i = 0; i < this.listeners.length; ++i) {
				if (this.listeners[i].end === this) {
					this.listeners[i].detachDependencies();

					if (this.listeners[i].end) {
						this.listeners[i].end.detachDependencies();
					}
				} else {
					this.listeners[i].changedDependencies.push(this);

					this.listeners[i].shouldUpdate = true;
					this.listeners[i].findDependencies();
				}
			}

			for (; nextStreamOrderIndex >= 0; --nextStreamOrderIndex) {
				if (streamOrder[nextStreamOrderIndex].shouldUpdate === true) {
					streamOrder[nextStreamOrderIndex].updateStream();
				}

				streamOrder[nextStreamOrderIndex].isQueued = false;
			}
		}
	}, {
		key: 'findDependencies',
		value: function findDependencies() {
			if (this.isQueued === false) {
				this.isQueued = true;

				for (var i = 0; i < this.listeners.length; ++i) {
					this.listeners[i].findDependencies();
				}

				streamOrder[++nextStreamOrderIndex] = this;
			}
		}
	}, {
		key: 'detachDependencies',
		value: function detachDependencies() {
			for (var i = 0; i < this.dependencies.length; ++i) {
				this.dependencies[i].listeners[this.dependencies[i].listeners.indexOf(this)] = this.dependencies[i].listeners[this.dependencies[i].listeners.length - 1];
				this.dependencies[i].listeners.length--;
			}

			this.dependencies.length = 0;
		}

		/**
   * Pushes `value` to the stream. If `value` is a `Promise` instance, it will be resolved first.
   * Method is always bound to the stream instance.
   *
   * @param {V | Promise<V>} value
   * @returns {this}
   */

	}, {
		key: 'get',


		/**
   * Returns current value of stream.
   *
   * @returns {*}
   */
		value: function get() {
			return this.value;
		}

		/**
   * Returns string representation of stream.
   *
   * @returns {string}
   */

	}, {
		key: 'toString',
		value: function toString() {
			return 'stream(' + this.value + ')';
		}

		/**
   * Sets up stream's dependencies. Only the last passed functino will be used as stream's function.
   * Stream's body function is called with following parameters: stream's dependencies, reference to the stream itself, and an array of changed dependencies.
   * This functon is only called when all dependencies have value. Returned value - anything but `undefined` - will trigger an update. To trigger on undefined, update directly with `push` method.
   *
   * @param {...(Function|Stream)}
   * @returns {this}
   *
   * @example
   * let stream1 = new Stream();
   * let stream2 = new Stream();
   * let stream3 = new Stream();
   * let stream4 = new Stream();
   *
   * stream3.combine((stream1Dependency, stream2Dependency, self, changed) => stream1Dependency.get() + stream2Dependency.get(), stream1, stream2);
   * stream4.combine((stream3Dependency, self, changed) => {
   * 	self.push(stream3Dependency.get() * 2);
   * }, stream3);
   *
   * stream1.push(2);
   * stream2.push(3);
   * stream3.get(); // -> 5
   * stream4.get(); // -> 10
   */

	}, {
		key: 'combine',
		value: function combine() {
			var i = 0;
			var dependencies = void 0;
			var dependenciesEndStreams = void 0;

			this.detachDependencies();

			if (this.end) {
				this.end.detachDependencies();
			}

			dependencies = [];
			dependenciesEndStreams = [];

			for (var _len = arguments.length, streams = Array(_len), _key = 0; _key < _len; _key++) {
				streams[_key] = arguments[_key];
			}

			if ((0, _isFunction2.default)(streams[i])) {
				this.fn = streams[i];
				i = 1;
			}

			for (; i < streams.length; ++i) {
				if (streams[i] !== undefined) {
					dependencies.push(streams[i]);

					if (streams[i].end) {
						dependenciesEndStreams.push(streams[i].end);
					}
				}
			}

			if (dependencies.length) {
				this.dependencies = dependencies;
				this.areDependenciesMet = false;
				this.changedDependencies = [];
				this.shouldUpdate = false;

				for (var _i = 0; _i < dependencies.length; ++_i) {
					dependencies[_i].listeners.push(this);
				}

				if (this.end) {
					this.end.listeners.push(this);
				}

				for (var _i2 = 0; _i2 < dependenciesEndStreams.length; ++_i2) {
					dependenciesEndStreams[_i2].listeners.push(this.end);
				}

				if (this.end) {
					this.end.dependencies = dependenciesEndStreams;
				}

				this.updateStream();
			}

			return this;
		}

		/**
   * Creates new dependent stream.
   *
   * @param {...(Function|Stream)}
   * @returns {Stream}
   *
   * @example
   * let newStream = Stream.combine((oldStreamDependency) => oldStreamDependency.get() * 2, oldStreamDependency);
   *
   * // same as
   * let newStream = new Stream();
   *
   * newStream.combine((oldStreamDependency) => oldStreamDependency.get() * 2, oldStreamDependency);
   */

	}, {
		key: 'immediate',


		/**
   * Immediately calls stream's body function, even if all dependencies don't have values yet.
   *
   * @returns {this}
   */
		value: function immediate() {
			if (!this.areDependenciesMet) {
				this.areDependenciesMet = true;

				this.updateStream();
			}

			return this;
		}

		/**
   * Creates new stream consisting of values returned by the function `fn` called with values from `stream`.
   *
   * @param {Function} fn
   * @param {Stream} stream
   * @returns {Stream}
   */

	}, {
		key: 'map',


		/**
   * Creates new stream consisting of values returned by the function `fn` called with values from stream instance.
   *
   * @param {Function} fn
   * @returns {Stream}
   */
		value: function map(fn) {
			return Stream.map(fn, this);
		}

		/**
   * Returns `true` if `value` is an instance of `Stream`.
   *
   * @param {*} value
   * @returns {boolean}
   */

	}, {
		key: 'endsOn',


		/**
   * Changes end stream.
   *
   * @param {Stream}
   * @returns {this}
   */
		value: function endsOn(endStream) {
			if (this.end) {
				this.end.detachDependencies();
				endStream.listeners.push(this.end);
				this.end.dependencies.push(endStream);
			}

			return this;
		}

		/**
   * Similar to `map`, but the returned stream is empty and is not updated.
   *
   * @param {Function} fn
   * @param {Stream} stream
   * @returns {Stream}
   */

	}, {
		key: 'on',


		/**
   * Similar to `map`, but the returned stream is empty and is not updated.
   *
   * @param {Function} fn
   * @returns {Stream}
   */
		value: function on(fn) {
			return Stream.on(fn, this);
		}

		/**
   * Similar to `on`, but the `fn` isn't called if `stream` already has value; only values pushed to `stream` after the `subscribe` was called are relevant.
   *
   * @param {Function} fn
   * @param {Stream} stream
   * @returns {Stream}
   */

	}, {
		key: 'subscribe',


		/**
   * Similar to `on`, but the `fn` isn't called if `stream` already has value; only values pushed to `stream` after the `subscribe` was called are relevant.
   *
   * @param {Function} fn
   * @returns {Stream}
   */
		value: function subscribe(fn) {
			return Stream.subscribe(fn, this);
		}

		/**
   * Creates new stream consisting of values from both `stream1` and `stream2`.
   *
   * @param {Stream} stream1
   * @param {Stream} stream2
   * @returns {Stream}
   */

	}, {
		key: 'ap',


		/**
   * Creates new stream consisting of values which are results of applying function from stream instance to the values of `stream`.
   *
   * @param {Stream} stream
   * @returns {Stream}
   */
		value: function ap(stream) {
			return Stream.combine(function (self, changed, dependency1) {
				if (dependency1.value) {
					self.push(dependency1.value(stream.value));
				}
			}, this, stream);
		}

		/**
   * Creates a stream, with initial value of `value`.
   *
   * @param {?*} value
   * @returns {Stream}
   */

	}], [{
		key: 'flushUpdate',
		value: function flushUpdate() {
			isFlushing = true;

			while (streamsToUpdate.length > 0) {
				var stream = streamsToUpdate.shift();

				if (stream) {
					if (stream.values.length > 0) {
						stream.value = stream.values.shift();
					}

					stream.updateDependencies();
				}
			}

			isFlushing = false;
		}
	}, {
		key: 'combine',
		value: function combine(fn) {
			var newStream = new Stream();

			for (var _len2 = arguments.length, streams = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
				streams[_key2 - 1] = arguments[_key2];
			}

			newStream.combine.apply(newStream, [fn].concat(streams));

			return newStream;
		}
	}, {
		key: 'map',
		value: function map(fn, stream) {
			return Stream.combine(function (self, changed, streamDependency) {
				self.push(fn(streamDependency.value));
			}, stream);
		}
	}, {
		key: 'isStream',
		value: function isStream(value) {
			return value instanceof Stream;
		}
	}, {
		key: 'on',
		value: function on(fn, stream) {
			return Stream.combine(function (self, changed, streamDependency) {
				fn(streamDependency.value);
			}, stream);
		}
	}, {
		key: 'subscribe',
		value: function subscribe(fn, stream) {
			var omitFirstRun = stream.hasValue;
			var hasRun = false;

			return Stream.combine(function (self, changed, dependency) {
				if (hasRun || !omitFirstRun && !hasRun) {
					fn(dependency.value);
				}

				hasRun = true;
			}, stream);
		}
	}, {
		key: 'merge',
		value: function merge(stream1, stream2) {
			var newStream = Stream.combine(function (self, changed, dependencyStream1, dependencyStream2) {
				if (changed[0]) {
					self.push(changed[0].get());
				} else if (dependencyStream1.hasValue) {
					self.push(dependencyStream1.value);
				} else if (dependencyStream2.hasValue) {
					self.push(dependencyStream2.value);
				}
			}, stream1, stream2).immediate();

			var endStream = Stream.combine(trueFn, stream1.end, stream2.end);

			newStream.endsOn(endStream);

			return newStream;
		}
	}, {
		key: 'of',
		value: function of(value) {
			return new Stream(value);
		}
	}]);
	return Stream;
}();

var _initialiseProps = function _initialiseProps() {
	var _this = this;

	this.listeners = [];
	this.dependencies = [];
	this.values = [];
	this.value = undefined;
	this.isQueued = false;
	this.areDependenciesMet = false;
	this.changedDependencies = [];
	this.shouldUpdate = false;
	this.hasValue = false;
	this.end = undefined;
	this.isEndStream = false;
	this.fn = undefined;

	this.push = function (value) {
		if (value !== undefined && value !== null && (0, _isFunction2.default)(value.then)) {
			value.then(_this.push);

			return _this;
		}

		_this.value = value;
		_this.hasValue = true;

		if (inStream === undefined) {
			isFlushing = true;

			_this.updateDependencies();

			if (streamsToUpdate.length > 0) {
				Stream.flushUpdate();
			} else {
				isFlushing = false;
			}
		} else if (inStream === _this) {
			for (var i = 0; i < _this.listeners.length; ++i) {
				if (_this.listeners[i].end === _this) {
					_this.listeners[i].detachDependencies();
					_this.listeners[i].end.detachDependencies();
				} else {
					_this.listeners[i].changedDependencies.push(_this);

					_this.listeners[i].shouldUpdate = true;
				}
			}
		} else {
			_this.values.push(value);
			streamsToUpdate.push(_this);
		}

		return _this;
	};
};

exports.default = Stream;