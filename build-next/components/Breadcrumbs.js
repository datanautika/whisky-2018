'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Breadcrumbs = require('./Breadcrumbs.css');

var _Breadcrumbs2 = _interopRequireDefault(_Breadcrumbs);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _whiskyName = require('../utils/whiskyName');

var _whiskyName2 = _interopRequireDefault(_whiskyName);

var _data = require('../data');

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = url => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new _BrowserRouter2.default(_router2.default);

let Breadcrumbs = function (_React$PureComponent) {
	_inherits(Breadcrumbs, _React$PureComponent);

	function Breadcrumbs() {
		var _temp, _this, _ret;

		_classCallCheck(this, Breadcrumbs);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (Breadcrumbs.__proto__ || Object.getPrototypeOf(Breadcrumbs)).call(this, ...args)), _this), _this.handleClick = event => {
			if (event.button !== 1) {
				let url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Breadcrumbs, [{
		key: 'render',
		value: function render() {
			var _routeStream$get = _routeStream2.default.get();

			let page = _routeStream$get.page,
			    subpage = _routeStream$get.subpage;

			let level1;
			let level2;

			if (page === 'uzivatele') {
				level1 = 'Uživatelé';
				level2 = subpage;
			}

			if (page === 'whisky') {
				level1 = 'Whisky';

				if (subpage) {
					let whisky = _lodash2.default.find(_data.whiskies, { id: subpage });

					if (whisky) {
						level2 = (0, _whiskyName2.default)(whisky).join(' ');
					}
				}
			}

			if (page === 'kategorie') {
				level1 = 'Kategorie';

				if (subpage) {
					level2 = _lodash2.default.find(_data.categories, { id: subpage }).name;
				}
			}

			return _react2.default.createElement(
				'div',
				{ className: _Breadcrumbs2.default.root, onClick: this.handleClick },
				_react2.default.createElement(
					'ol',
					null,
					_react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('') },
							'\u25CA'
						)
					),
					level1 ? _react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)(page) },
							level1
						)
					) : null,
					level2 && (typeof subpage === 'number' || page !== 'whisky') ? _react2.default.createElement(
						'li',
						null,
						level2
					) : null
				),
				_react2.default.createElement(
					'nav',
					null,
					_react2.default.createElement(
						'ol',
						null,
						_react2.default.createElement(
							'li',
							null,
							_react2.default.createElement(
								'a',
								{ href: (0, _href2.default)('uzivatele') },
								'U\u017Eivatel\xE9'
							)
						),
						_react2.default.createElement(
							'li',
							null,
							_react2.default.createElement(
								'a',
								{ href: (0, _href2.default)('kategorie') },
								'Kategorie'
							)
						),
						_react2.default.createElement(
							'li',
							null,
							_react2.default.createElement(
								'a',
								{ href: (0, _href2.default)('whisky') },
								'Whisky'
							)
						)
					)
				)
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			_routeStream2.default.subscribe(() => {
				this.forceUpdate();
			});
		}
	}]);

	return Breadcrumbs;
}(_react2.default.PureComponent);

exports.default = Breadcrumbs;